package com.alticeusa.movetransfer.dao;

import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import java.sql.SQLException;
import java.sql.Types;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.movetransfer.data.*;
import com.alticeusa.movetransfer.exception.InternalServerErrorException;
import com.alticeusa.movetransfer.metrics.GetMoveTransferLoggingData;

/**
 * The class generates object of the MoveTransferDetail type that contain the 
 * information of old account number and move type
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-11-14
 */

public class GetMoveTransferDetailDao {

	private static final Logger logger = LogManager.getLogger(GetMoveTransferDetailDao.class);

	 
	/*
	 * This methods makes a call to SP based on input provided and returns
	 * MoveTransferDetail Object
	 * 
	 * @param zoomOrderId of the customer.
	 * 
	 * @return old account number and move type
	 */

	public GetMoveTransferResponse getMoveTransferDetail(long zoomOrderId) {
		logger.info(">>>>> In DAO GetMoveTransferDetail for zoomOrderId: " + zoomOrderId);
		
		Connection con = null;

	    CallableStatement csmt = null;

		
		GetMoveTransferResponse moveTransferResponse = new GetMoveTransferResponse();
		GetMoveTransferRequest moveTransferRequest = new GetMoveTransferRequest();
		moveTransferRequest.setZoomOrderId(zoomOrderId);
		GetMoveTransferConnectionDao getMoveTransferConnectionDao = null;	

		try {
			getMoveTransferConnectionDao = com.alticeusa.movetransfer.dao.GetMoveTransferConnectionDao.getInstance();

			logger.debug("Getting DB connection.");

			con = getMoveTransferConnectionDao.getDBConnection();

			logger.debug("DB connected.");

			csmt = con.prepareCall("{call sigma_order_svcs.get_move_transfer_info(?,?,?)}" );

			csmt.setLong(1, zoomOrderId);

			csmt.registerOutParameter(2,Types.VARCHAR);
			
			csmt.registerOutParameter(3,Types.VARCHAR);
			logger.info(">>>>> Calling SP get_move_transfer_info");
			csmt.executeUpdate();
			logger.info("<<<<< SP get_move_transfer_info is called");
			
			String oldAccNumOut = csmt.getString(2);
			String moveTypeOut = csmt.getString(3);
			
			moveTransferResponse.setOldAccountNumber(oldAccNumOut);
	    	moveTransferResponse.setMoveType(moveTypeOut);

			logger.info("<<<<<< Executed Stroed Procedure get_move_transfer_info ");
		}

		catch (Exception e) {

			logger.error("Exception occured while calling SP get_move_transfer_info():", e);
			logger.info("<<<<< Throwing Internal Server Exception for the zoom order id: " + zoomOrderId);
			throw new InternalServerErrorException();

		}

		finally {
			if (con != null) {
				try {
					csmt.close();
				} catch (SQLException e) {
					logger.error("SQLException occured while closing DB connection:", e);
				}
			}
			
			if (csmt != null) {
				try {
					csmt.close();
				} catch (SQLException e) {
					logger.error("SQLException occured while closing DB connection:", e);
				}
			}
			performMetricsLogging(moveTransferRequest, moveTransferResponse);
		}
		logger.info("<<<<< Out DAO get_move_transfer_info for zoom order id: " + zoomOrderId);
		return moveTransferResponse;
	}
	
	/*
	 * This methods used for Metrics logging.
	 * It give the response and request in XML format in logs.
	 * 
	 * @param GetMoveTransferRequest, GetMoveTransferResponse
	 * 
	 * @return void
	 */

	private void performMetricsLogging(GetMoveTransferRequest moveTransferRequest , GetMoveTransferResponse moveTransferResponse)
	  {
		
		  String xmlRequest="";
		  String xmlResponse="";
		  GetMoveTransferLoggingData metricsData = new GetMoveTransferLoggingData(moveTransferRequest, moveTransferResponse);
		  
		  try {
		  JAXBContext jaxbContext = JAXBContext.newInstance(GetMoveTransferRequest.class);
		  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		  // output pretty printed
		 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter stringRequest = new StringWriter();
			jaxbMarshaller.marshal(metricsData.getRequest(), stringRequest);
			xmlRequest = stringRequest.toString();
		  
		  } catch (PropertyException e) {
				logger.error("Exception occured while setting property for jaxbMarshaller in performMetricsLogging Method  :", e);
		  } catch (JAXBException e) {
			    logger.error("JAXBException occured for jaxbMarshaller in performMetricsLogging Method:", e);
		  }
		  
		  try {
			  JAXBContext jaxbContext = JAXBContext.newInstance(GetMoveTransferResponse.class);
			  Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				StringWriter stringResponse = new StringWriter();
				jaxbMarshaller.marshal(metricsData.getResponse(), stringResponse);
				xmlResponse = stringResponse.toString();
			  
			  } catch (PropertyException e) {
					logger.error("Exception occured while setting property for jaxbMarshaller in performMetricsLogging Method  :", e);
			  } catch (JAXBException e) {
				    logger.error("JAXBException occured for jaxbMarshaller in performMetricsLogging Method:", e);
			 }
		  
		  
		  logger.info("Request for GetMoveTransferDetail:"+xmlRequest);
		  logger.info("Response for GetMoveTransferDetail:"+xmlResponse);
	  } 
}
