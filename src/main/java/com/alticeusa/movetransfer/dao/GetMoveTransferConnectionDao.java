package com.alticeusa.movetransfer.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The class helps to get connected to the database And output is connection
 * class object
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-10-27
 */

public class GetMoveTransferConnectionDao extends BaseDao {

	private static final Logger logger = LogManager.getLogger(GetMoveTransferConnectionDao.class);

	public static GetMoveTransferConnectionDao instance = null;

	/**
	 * This methods makes an instance of GetCustomerInfoConnectionDao.
	 * 
	 * @return instance
	 * @throws Exception
	 */
	public static GetMoveTransferConnectionDao getInstance() throws Exception {
		if (instance == null) {
			instance = new GetMoveTransferConnectionDao();
		} // if
		return instance;
	}

	/*
	 * This methods makes a DB connection for datasource passed.
	 * 
	 * @return Connection object
	 * 
	 * @throws Exception
	 */
	public Connection getDBConnection() throws ClassNotFoundException, SQLException {
		Connection dbConnection = null;
		try {
			dbConnection = getConnection(VOICESDL_DS);
		} catch (Exception e) {
			if (e.getMessage() != null)
				logger.debug(
						"Error creating connection from DataSource :" + VOICESDL_DS + " Exception :" + e.getMessage());
			logger.debug("Error creating connection from DataSource :" + VOICESDL_DS);
		}
		return dbConnection;
	}
}
