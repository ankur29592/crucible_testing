package com.alticeusa.movetransfer.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.movetransfer.utils.StringFormatter;

/**
 * This class used to establish the DB connections.
 * 
 * @author Ayush Yadav
 * @version 1.0
 * @since 2017-10-27
 */

public abstract class BaseDao implements DataSources
{
	private static final String ENV_CONTEXT = "java:comp/env";
	private static final Logger logger = LogManager.getLogger(BaseDao.class);
	private boolean transFlag = false; // TransactionFlag
		
	/**
	 * This methods makes a look up for the datasource for DB connection.
	 * @return
	 * @throws DbException
	 */
	public Connection getConnection(String DATASOURCE) throws Exception 
	{
		DataSource ds = null;
		Context ctx = null;
		Connection abstractConnection = null;
		try 
		{
			ctx = new InitialContext();
			Context envContext  = (Context)ctx.lookup(ENV_CONTEXT);
			ds = (javax.sql.DataSource) envContext.lookup(DATASOURCE);
			abstractConnection = ds.getConnection();
		}// try
		catch (Exception e) 
		{
			throw e;
		}// catch
		finally
		{
		    close (ctx);
		}
		return abstractConnection;
	}// getConnection
	
 	public int handleInvalidPackageException(SQLException se,int numberOfRetries,List<String> errorCodesToCheck,long sleepTime) throws SQLException,Exception{
		String message = se.getMessage();
		boolean checkError=StringFormatter.checkStringContainsList(message,errorCodesToCheck);
		if(checkError) {
			numberOfRetries--;
			if(numberOfRetries<0) {
				logger.debug("no more retries");
				throw se;
			}else {
				logger.debug("retrying again numberLeft {}",numberOfRetries);
				logger.debug("thread sleep for {} ms",sleepTime);
				Thread t = new Thread();
				t.sleep(sleepTime);
				logger.debug("after thread sleep");
			}
		}else {
			numberOfRetries = -1;
			throw se;
		}
		return numberOfRetries;
  	}
	/**
	 * This method closes the <code>context</code>.
	 * @param context
	 */
	public void close (Context context)
	{
        try
        {
            context.close();
        }//try
        catch (NamingException e)
        {
         // Do nothing in this case.
        }//catch
	}//close
	
	/**
	 * This method closes the <code>connection</code>,<code>statement</code>,
	 * <code>resultSet</code>
	 * @param connection
	 * @param statement
	 * @param resultSet
	 */
	public void close(Connection connection, Statement statement,ResultSet resultSet) 
	{
		try 
		{
			if (resultSet != null) 
			{
				resultSet.close();
				resultSet = null;
			}// if

			if (statement != null) 
			{
				statement.close();
				statement = null;
			}// if

			if (connection != null) 
			{
				connection.close();
				connection = null;

				
			}// if
		}// try
		catch (SQLException e) 
		{
			e.printStackTrace();
		}// catch
	}// close

	public void finalize(Connection connection, Statement statement, ResultSet resultSet) 
	{
		transFlag = false;
		close(connection, statement, resultSet);
	}// finalize

	public Connection startTransaction(String dataSource) throws Exception 
	{
	    if (!transFlag) 
	    {
	    	Connection abstractConnection = getConnection(dataSource);
	    	abstractConnection.setAutoCommit(false);
			transFlag = true;
			return abstractConnection;
		}// if
		return null;
	}// startTransaction
}//BaseDAO