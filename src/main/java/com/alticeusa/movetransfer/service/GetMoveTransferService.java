package com.alticeusa.movetransfer.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alticeusa.movetransfer.dao.GetMoveTransferDetailDao;
import com.alticeusa.movetransfer.data.GetMoveTransferResponse;
import com.alticeusa.movetransfer.exception.InternalServerErrorException;
import com.alticeusa.movetransfer.utils.Constants;

/**
 * The class is the main service class that accept parameter ZoomOrderId 
 * from the URL and call the GetMoveTransferDetailDao to obtain customer
 * information calling SP. And output is GetMoveTransferDetail class object that
 * contain the details of a move transfer order in Zoom
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-11-14
 */
@Path("/api/v1")
public class GetMoveTransferService {

	private static final Logger logger = LogManager.getLogger(GetMoveTransferService.class);

	/*
	 * This methods makes a call to function getMoveTransferDetail of class
	 * GetMoveTransferDetailDao to get move transfer Information.
	 * 
	 * @param ZoomOrderId of the customer.
	 * 
	 * @return OldAccountNumber and MoveType
	 */
	@GET
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public GetMoveTransferResponse getMoveTransfer(@QueryParam ("zoomOrderId") long zoomOrderId) {
		logger.info("Method :" + Constants.METHOD_NAME + " : Invoked API : /getMoveTransferDetails/api/v1?zoomOrderId="+zoomOrderId);
		GetMoveTransferResponse moveTransferResponse = new GetMoveTransferResponse();
		
		try {
			GetMoveTransferDetailDao getMoveTransferDetailDao = new GetMoveTransferDetailDao();

			moveTransferResponse = getMoveTransferDetailDao.getMoveTransferDetail(zoomOrderId);
			
		}catch(WebApplicationException e) 
		{
			throw e;
		}
		catch (Exception e) {
			logger.info("<<<<< Throwing Internal Server Exception for the zoomOrderId: " + zoomOrderId);
			throw new InternalServerErrorException();
		}

		logger.info("<<<<< Finished API Execution : /getMoveTransferDetails/api/v1?zoomOrderId="+zoomOrderId);
		
		return moveTransferResponse;

	}
}
