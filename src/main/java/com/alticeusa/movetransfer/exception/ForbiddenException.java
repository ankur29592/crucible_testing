package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;

/**
 * The class is Exception Class to Create a HTTP 403 (Forbidden) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class ForbiddenException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 403 (Forbidden) exception.
	 */
	public ForbiddenException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_403).build());
	}

	/**
	 * Create a HTTP 403 (Forbidden) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 403 response.
	 */
	public ForbiddenException(int statusCode, String message) {
		super(Response.status(403).entity(message).type("text/plain").build());
	}

}