package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;

/**
 * The class is Exception Class to Create a HTTP 400 (Bad Request) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class BadRequestException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 400 (Bad Request) exception.
	 */
	public BadRequestException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_400).build());
	}

	/**
	 * Create a HTTP 400 (Bad Request) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 400 response.
	 */
	public BadRequestException(int statusCode, String message) {
		super(Response.status(400).entity(message).type("text/plain").build());
	}

}