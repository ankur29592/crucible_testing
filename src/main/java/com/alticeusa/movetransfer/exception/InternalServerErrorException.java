package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;


/**
 * The class is Exception Class to Create a HTTP 500 (Internal Server Error) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class InternalServerErrorException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 500 (Internal Server Error) exception.
	 */
	public InternalServerErrorException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_500).build());
	}

	/**
	 * Create a HTTP 500 (Internal Server Error) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 500 response.
	 */
	public InternalServerErrorException(int statusCode, String message) {
		super(Response.status(500).entity(message).type("text/plain").build());
	}

}