package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;


/**
 * The class is Exception Class to Create a HTTP 401 (Unauthorized User) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class UnathorizedUserException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 401 (Unauthorized User) exception.
	 */
	public UnathorizedUserException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_401).build());
	}

	/**
	 * Create a HTTP 401 (Unauthorized User) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 401 response.
	 */
	public UnathorizedUserException(int statusCode, String message) {
		super(Response.status(401).entity(message).type("text/plain").build());
	}

}