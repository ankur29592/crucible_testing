package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;


/**
 * The class is Exception Class to Create a HTTP 405 (Method not allowed) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class MethodNotAllowedException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 405 (Method not allowed) exception.
	 */
	public MethodNotAllowedException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_405).build());
	}

	/**
	 * Create a HTTP 405 (Method not allowed) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 405 response.
	 */
	public MethodNotAllowedException(int statusCode, String message) {
		super(Response.status(405).entity(message).type("text/plain").build());
	}

}