package com.alticeusa.movetransfer.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.alticeusa.movetransfer.utils.Constants;

/**
 * The class is Exception Class to Create a HTTP 204 (No Content) exception
 * 
 * @author Amit Rupanwar
 * @version 1.0
 * @since 2017-10-27
 */

public class NoContentException extends WebApplicationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a HTTP 204 (No Content) exception.
	 */
	public NoContentException() {
		super(Response.status(Constants.FAILURE_ERROR_CODE_204).build());
	}

	/**
	 * Create a HTTP 204 (No Content) exception.
	 * 
	 * @param message
	 *            the String that is the entity of the 204 response.
	 */
	public NoContentException(int statusCode, String message) {
		super(Response.status(204).entity(message).type("text/plain").build());
	}

}