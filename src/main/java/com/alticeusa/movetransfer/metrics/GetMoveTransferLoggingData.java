package com.alticeusa.movetransfer.metrics;

import com.alticeusa.movetransfer.data.GetMoveTransferResponse;
import com.alticeusa.movetransfer.data.GetMoveTransferRequest;

public class GetMoveTransferLoggingData {
	
	private final GetMoveTransferRequest request;
	private final GetMoveTransferResponse response;
	
	public GetMoveTransferRequest getRequest() {
		return request;
	}

	public GetMoveTransferResponse getResponse() {
		return response;
	}
	
	public GetMoveTransferLoggingData(GetMoveTransferRequest request, GetMoveTransferResponse response) {
		this.request = request;
		this.response = response;

	}
	
	
}
