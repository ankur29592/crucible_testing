package com.alticeusa.movetransfer.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is POJO class for GetSigmaHSDDetailsRequest class.
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-10-27
 */
@XmlRootElement
public class GetMoveTransferRequest {

	private long zoomOrderId;
	
	public long getZoomOrderId() {
		return zoomOrderId;
	}
	public void setZoomOrderId(long zoomOrderId) {
		this.zoomOrderId = zoomOrderId;
	}

}