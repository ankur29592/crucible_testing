package com.alticeusa.movetransfer.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
/*
 * This is a pojo class that contains the structure of the MoveTransfer output
 */

/**
 * This is POJO class for GetSigmaHSDDetailsRequest class.
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-10-27
 */
public class GetMoveTransferResponse {

	
	public GetMoveTransferResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String oldAccountNumber;
	private String moveType;
	
	public GetMoveTransferResponse(String oldAccountNumber, String moveType) {
		super();
		this.oldAccountNumber = oldAccountNumber;
		this.moveType = moveType;
	}
	
	public String getOldAccountNumber() {
		return oldAccountNumber;
	}
	
	public void setOldAccountNumber(String oldAccountNumber) {
		this.oldAccountNumber = oldAccountNumber;
	}
	
	public String getMoveType() {
		return moveType;
	}
	
	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}
	
}
