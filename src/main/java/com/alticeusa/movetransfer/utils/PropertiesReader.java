package com.alticeusa.movetransfer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class PropertiesReader
{
	private static final Logger logger = LogManager.getLogger(PropertiesReader.class);
    private Properties props;
    private static PropertiesReader singleton = null;
    
	public static PropertiesReader getInstance() 
	{
		if (singleton == null)
		{
			singleton = new PropertiesReader();
		}
		return singleton;
	}
	
	
    public PropertiesReader()
    {
    	
    	 InputStream is = PropertiesReader.class.getClassLoader().getResourceAsStream("sdlSigma.properties");
         props = new Properties();
         try
         {
             props.load(is);
         }
         catch (IOException e)
         {
             logger.error("Unable to Load Property File: " + e.getMessage());
         }
         catch (Exception ex)
         {
             logger.error("Unable to Load Property File: " + ex.getMessage());
         }
    }
      
	/**
	 * @return
	 */
	public Properties getParams()
    {
        return props ;
    }//getParams 
	
	/**
	 * @param key
	 * @return
	 */
	public String getValue(String key)
	{
		return props.getProperty(key);
	}//getValue
	
	public void printProperties()
	{
		if (null != props)
		{
			for (Enumeration<?> en = props.keys(); en.hasMoreElements();)
			{
				String key = (String)en.nextElement();
				//System.out.println(StringFormatter.formatString("key = {0}, value = {1}", key, props.get(key)));
				System.out.println("key :"+key.toString()+" value: "+props.get(key).toString());
			}//for
		}//if
	}
	
	
	public Properties filterPropertiesByPrefix(String prefix) 
	{
		Properties filteredProperties = new Properties();
		if (null != props) 
		{
			for (Enumeration<?> en = props.keys(); en.hasMoreElements();) 
			{
				String key = (String) en.nextElement();
				if (key.startsWith(prefix)) 
				{
					filteredProperties.put(key, props.get(key));
				}//if
			}//for
		}//if
		return filteredProperties;
	}//
}//PropertiesReader