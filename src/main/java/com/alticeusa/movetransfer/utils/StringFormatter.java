package com.alticeusa.movetransfer.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.alticeusa.sdl.sigma.message.SigmaAction;
import com.alticeusa.sdl.sigma.message.SigmaEvent;

/**
 * This class is for generating Validating Strings records.
 * 
 * @author Shivangi Malviya
 * @version 1.0
 * @since 2017-11-17
 */

public class StringFormatter {
	public static boolean checkNullString(SigmaEvent request) {
			if (nullOrBlank(request.getAccountNumber()) && nullOrBlank(request.getMessage()) && nullOrBlank(request.getWorkOrderType())) {
				return false;
			}
		return true;
	}
	public static boolean checkNullStringResponse(ArrayList<SigmaAction> arrayList) {
		 Iterator<SigmaAction> itr = arrayList.iterator();
			while(itr.hasNext()) {
				SigmaAction sa = itr.next();
				if(nullOrBlank(sa.getSigmaMessage().toString())) {
					return false;
				}
				
			} 
 
			return true;		
		}

	
	public static boolean isValidRateCodes(ArrayList<String> inputArray)
	{
		ArrayList<String> rateCodes = inputArray;
		int count=0;
		for(String rateCode : rateCodes)
		{
			if(isValidRateCode(rateCode))
			{
				count = 0;
			}			
			else{
				count++;
				}
		}
		
		if(count != 0)
		{
			return false;
		}
		return true;
	}
	
	
	
	public static boolean isValidRateCode(String input){
		String[] values = new String[]{"OPT1","OPT2","OPT3","OPT4","OPT5","OPT6","OPT7","OPT8","OPT9","OPT10",
		          "SDL1","SDL2","SDL3","SDL4","SDL5","SDL6","SDL7","SDL8","SDL9","SDL10"};
		return Arrays.asList(values).contains(input);
	}
	

	public static boolean checkNullArraylist(ArrayList<String> inputArraylist) {
		if (inputArraylist == null || inputArraylist.isEmpty()) {
			return false;
		}
		return true;
	}
		
	private static boolean nullOrBlank(String inputString) {
		
		return (inputString == null || (inputString.trim().equalsIgnoreCase(""))
				|| (inputString.trim().length() == 0)
				|| (inputString.trim().equalsIgnoreCase("null")));
	}

	
	public static boolean isNumeric(String field) {
		if(field.matches("[0-9]+")) {
			return true;
		}
		return false ;
	}
	
	
	
	
	public static boolean isValidSourceApp(String input){
		if(input.equalsIgnoreCase("IDA") || input.equalsIgnoreCase("ECOMMERCE")){
			return true;
		}
		return false;
	}
	
	
	
	public static boolean isValidFootPrint(String input){
		if(input.equalsIgnoreCase("OPT") || input.equalsIgnoreCase("SDL")){
			return true;
		}
		return false;
	}
	
	
	public static boolean toBoolean(String inputString)
    {
		return (inputString == null || (inputString.trim().equalsIgnoreCase(""))
				|| (inputString.trim().length() == 0)
				|| (inputString.trim().equalsIgnoreCase("null")));
    }

	

	public static String getFormattedDate(long inputDate,SimpleDateFormat outputDateFormat)
	{
		String outputDate=null;
		try
		{
			
			Date d = new Date(inputDate);
			if(outputDateFormat!=null)
				outputDate =  outputDateFormat.format(d);
		}
		catch(Exception e)
		{
		}
		return outputDate;
	}
	
	public static boolean checkStringContainsList(String str,List<String> stringList) {
		if(str!=null && stringList!=null && stringList.size()>0) {
			for(String string:stringList) {
				if(str.contains(string)) {
					return true;
				}
			}
		}else {
			return false;
		}
		return false;
	}

	public static char[] formatString(String string, String key, Object object) {
		return null;
	}
	
}
