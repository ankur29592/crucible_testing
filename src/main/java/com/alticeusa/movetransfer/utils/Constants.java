package com.alticeusa.movetransfer.utils;

public class Constants {

	public static final String METHOD_NAME = "getMoveTransferDetails"; 
	public static final String SUCCESS = "SUCCESS";
	public static final int FAILURE_ERROR_CODE_400 = 400;
	public static final int FAILURE_ERROR_CODE_401 = 401;
	public static final int FAILURE_ERROR_CODE_403 = 403;
	public static final int FAILURE_ERROR_CODE_404 = 404;
	public static final int FAILURE_ERROR_CODE_405 = 405;
	public static final int FAILURE_ERROR_CODE_204 = 204;
	public static final int FAILURE_ERROR_CODE_500 = 500; 
}
